//
//  ViewController.swift
//  ArticleApp
//
//  Created by hongly on 12/14/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit
import Kingfisher

class ArticleViewController: UIViewController {

    //Mark: Outlets
    @IBOutlet weak var articleTableView: UITableView!
    
    //Mark: Properties
    var articlePresenter = ArticlePresenter()
    var articles = [Article]()
    var page = Page.shared
    let refreshControl = UIRefreshControl()
    var isLoadMore = false
    
    //Mark: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        //Article Presenter
        articlePresenter.delegate = self
        
        //Table View
        articleTableView.delegate = self
        articleTableView.dataSource = self
        
        // Add Refresh Control to Table View
        refreshControl.addTarget(self, action: #selector(pullToRefresh), for: .valueChanged)
        if #available(iOS 10.0, *) {
            articleTableView.refreshControl = refreshControl
        } else {
            articleTableView.addSubview(refreshControl)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        //Get data when view is loaded
        isLoadMore = false
        page.currentPage = 1
        articlePresenter.getAllArticles(page: page)
    }

    //Mark: Function
    @objc func pullToRefresh(){
        page.currentPage = 1
        articlePresenter.getAllArticles(page: page)
        self.refreshControl.endRefreshing()
    }
}


extension ArticleViewController: ArticlePresenterProtocol {
    func didInsertOneArtilce(article: Article) {
        
    }
    
    func didUpdateOneArtilce(article: Article) {
        
    }
    
    func didUploadImage(imageUrl: String) {
        
    }
    
    func didResponseArticles(articles: [Article]) {
        if isLoadMore {
            self.articles += articles
        }else {
           self.articles = articles
        }
        
        self.articleTableView.reloadData()
    }
    
    func didDeleteOneArtilce(article: Article) {
        self.articleTableView.reloadData()
    }
    
    func didResponseOneArtilce(article: Article) {
        
    }
}

extension ArticleViewController: UITableViewDataSource, UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "detailViewController") as! DetailViewController
        vc.article = articles[indexPath.row]
     
        self.navigationController?.pushViewController(vc, animated: true)
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return articles.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        tableView.register(UINib(nibName: "ArticleTableViewCell", bundle: nil), forCellReuseIdentifier: "article_cell")
        let cell = tableView.dequeueReusableCell(withIdentifier: "article_cell") as! ArticleTableViewCell
        cell.titleLabel.text = articles[indexPath.row].title
        
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "yyyyMMddHHmmss"
            let formatedDate = dateFormatter.date(from: articles[indexPath.row].created_date!)
            dateFormatter.dateFormat = "dd/MM/yyyy"
        cell.dateLabel.text = dateFormatter.string(from: formatedDate!)
        if let image = articles[indexPath.row].imageUrl {
            cell.articleImageView.kf.setImage(with: URL(string: image) ,placeholder: UIImage(named: "img_placeholder"),options: [
                .transition(.fade(0.5))
                ])
        }else {
            cell.articleImageView.image = UIImage(named: "img_placeholder")
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, leadingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {
        
        let editAction = UIContextualAction(style: .normal, title:  "Edit", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in
            
            let vc = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "uploadViewController") as! PostViewController
            vc.article = self.articles[indexPath.row]
            vc.isUpdate = true
            self.navigationController?.pushViewController(vc, animated: true)
            success(true)
        })
        editAction.image = UIImage(named: "edit")
        editAction.backgroundColor = UIColor.green
        return UISwipeActionsConfiguration(actions: [editAction])
    }
    
    func tableView(_ tableView: UITableView, trailingSwipeActionsConfigurationForRowAt indexPath: IndexPath) -> UISwipeActionsConfiguration? {

        let deleteAction = UIContextualAction(style: .normal, title:  "Delete", handler: { (ac:UIContextualAction, view:UIView, success:(Bool) -> Void) in

            let id = self.articles[indexPath.row].id
            self.articlePresenter.deleteOneArticleById(id: id!)
            self.articles.remove(at: indexPath.row)
      
            success(true)
        })
        deleteAction.image = UIImage(named: "delete")
        deleteAction.backgroundColor = .red

        return UISwipeActionsConfiguration(actions: [deleteAction])
    }
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        
        isLoadMore = true
        let lastElement = articles.count - 1
        if indexPath.row == lastElement {
            page.currentPage += 1
            articlePresenter.getAllArticles(page: page)
        }
    }
}

