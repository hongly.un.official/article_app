//
//  VDetailViewController.swift
//  ArticleApp
//
//  Created by hongly on 12/21/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit

class DetailViewController: UIViewController, UIGestureRecognizerDelegate {

    //MARK: Outlets
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var authorNameLabel: UILabel!
    @IBOutlet weak var articleImageView: UIImageView!
    @IBOutlet weak var descriptionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    //MARK: Properties
    var article: Article?
    
    //View Controller Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        
        titleLabel.text = self.article!.title
        authorNameLabel.text = "Jason"
        descriptionLabel.text = self.article!.description
        
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "yyyyMMddHHmmss"
        let formatedDate = dateFormatter.date(from: article!.created_date!)
        dateFormatter.dateFormat = "dd/MM/yyyy"
        dateLabel.text = dateFormatter.string(from: formatedDate!)
        if article!.imageUrl != nil {
            articleImageView.kf.setImage(with: URL(string: self.article!.imageUrl!), placeholder: UIImage(named: "img_placeholder"))
        }else {
            articleImageView.image = UIImage(named: "img_placeholder")
        }
        
        articleImageView.isUserInteractionEnabled = true
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(handleLongPress))
        lpgr.minimumPressDuration = 0.5
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.articleImageView.addGestureRecognizer(lpgr)
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        
        let alert = UIAlertController(title: "Save Image", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Save", style: .default, handler: { (_) in
            do {
                let image = UIImage(data: try Data(contentsOf: URL(string: ((self.article?.imageUrl!)!))!))
                UIImageWriteToSavedPhotosAlbum(image!, nil, nil, nil);
            }catch let err {
                print("Error Save Image \(err)")
                return
            }
        }))
        alert.addAction(UIAlertAction(title: "Cancel", style: .default, handler: { (_) in
            return
        }))
        
        self.present(alert, animated: true)
    }
}
