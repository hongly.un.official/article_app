//
//  UploadViewController.swift
//  ArticleApp
//
//  Created by hongly on 12/17/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import UIKit
import SwiftyJSON

class PostViewController: UIViewController {

    
    //MARK: Outlets
    @IBOutlet weak var uploadImageView: UIImageView!
    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var descriptionTextView: UITextView!
//    @IBOutlet weak var uploadProgressView: UIProgressView!
    
    //MARK: Properties
    var articlePresenter = ArticlePresenter()
    var isUpdate:Bool = false
    var imageIsPicked: Bool = false
    var article: Article?

    //MARK: ViewController life cycle
    override func viewDidLoad() {
        super.viewDidLoad()

        articlePresenter.delegate = self
        
        //Setup and Add TapGesture to uploadImageView
            setUpGestureForImageView()
        //Setup ProgressView
//            setUpUploadProgressView()
        
            if isUpdate {
                titleTextField.text = article!.title
                descriptionTextView.text = article!.description
                if article?.imageUrl != nil {
                    uploadImageView.kf.setImage(with: URL(string: article!.imageUrl!), placeholder: UIImage(named: "img_placeholder"), options:[.transition(.fade(0.5))])
                }
            }
    }
    
    //Defined Function
    func setUpGestureForImageView() {
        let tapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(selectImageTapped))
        uploadImageView.isUserInteractionEnabled = true
        uploadImageView.addGestureRecognizer(tapGestureRecognizer)
        
    }
    
//    func setUpUploadProgressView() {
//        uploadProgressView.progress = 0.0
//        uploadProgressView.isHidden = true
//    }
    
    func camera(){
        if UIImagePickerController.isSourceTypeAvailable(.camera){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .camera
            self.present(myPickerController, animated: true, completion: nil)
        }
    }
    
    func photoLibrary(){
        if UIImagePickerController.isSourceTypeAvailable(.photoLibrary){
            let myPickerController = UIImagePickerController()
            myPickerController.delegate = self;
            myPickerController.sourceType = .photoLibrary
            self.present(myPickerController, animated: true, completion: nil)
        }
    }

    
    //MARK: Action and @IBAction
    @objc func selectImageTapped(tapGestureRecognizer: UITapGestureRecognizer)
    {
        let alert = UIAlertController(title: "Select Image", message: "", preferredStyle: .actionSheet)
        alert.addAction(UIAlertAction(title: "Camera", style: .default, handler: { (action) in
            self.camera()
        }))
        alert.addAction(UIAlertAction(title: "Gallary", style: .default, handler: { (action) in
            self.photoLibrary()
        }))
        present(alert, animated: true)
    }
    
    @IBAction func uploadButtonTapped(_ sender: UIButton) {
        
        let image = uploadImageView.image
        
        if isUpdate == false {
         
            articlePresenter.uploadImage(image: image!)

        }else if imageIsPicked {
            
            articlePresenter.uploadImage(image: image!)
            
        }else {
            let id = self.article?.id
            let title = self.titleTextField.text
            let description = self.descriptionTextView.text
            let category = Category(id: 1, name: nil)
            let imageUrl = self.article?.imageUrl
            
            let article = Article(id: id, title: title, description: description, created_date: nil, category: category, author: nil, imageUrl: imageUrl, status: "1")
        
            articlePresenter.updateArticleById(article: article)
        }
    }
}

extension PostViewController: ArticlePresenterProtocol {
    func didInsertOneArtilce(article: Article) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func didResponseArticles(articles: [Article]) {
        
    }
    
    func didResponseOneArtilce(article: Article) {
        
    }
    
    func didDeleteOneArtilce(article: Article) {
        
    }
    
    func didUpdateOneArtilce(article: Article) {
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    func didUploadImage(imageUrl: String) {
        
        let title = self.titleTextField.text
        let description = self.descriptionTextView.text
        let category = Category(id: 1, name: nil)
    
        if !isUpdate {
            let article = Article(id: nil, title: title, description: description, created_date: nil, category: category, author: nil, imageUrl: imageUrl, status: "1")
            articlePresenter.instertOneArticle(article: article)
        }else {
            let id = self.article?.id
            let article = Article(id: id, title: title, description: description, created_date: nil, category: category, author: nil, imageUrl: imageUrl, status: "1")
            articlePresenter.updateArticleById(article: article)
        }
        
    }
    
}

extension PostViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        let img = info[UIImagePickerController.InfoKey.originalImage] as! UIImage
        uploadImageView.image = img
        imageIsPicked = true
        dismiss(animated: true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        self.dismiss(animated: true, completion: nil)
    }
}
