//
//  Article.swift
//  ArticleApp
//
//  Created by hongly on 12/14/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import SwiftyJSON
class Article {
    var id: Int?
    var title: String?
    var description: String?
    var created_date: String?
    var category: Category?
    var author: Author?
    var imageUrl:String?
    var status: String?
    
    init(){}
    init(id: Int?, title: String?, description: String?, created_date: String?, category: Category?, author: Author?, imageUrl: String?, status: String?) {
        self.id = id
        self.title = title
        self.description = description
        self.created_date = created_date
        self.category = category
        self.author = author
        self.imageUrl = imageUrl
        self.status = status
    }
    
    init(json: JSON) {
        id = json["ID"].int!
        title = json["TITLE"].string
        description = json["DESCRIPTION"].string
        created_date = json["CREATED_DATE"].string
        category = Category(json: json["CATEGORY"])
        imageUrl = json["IMAGE"].string
        status = json["STATUS"].string
    }
}
