//
//  ArticleController.swift
//  ArticleApp
//
//  Created by hongly on 12/14/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

class ArticleModelController  {
    
    static var shared = ArticleModelController()

    func getAllArticles(page: Page,completion: @escaping ([Article], Bool) -> Void){
        let url = "http://www.api-ams.me/v1/api/articles?page=\(page.currentPage)&limit=\(page.limit)"
        let headers = ["Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        var articles = [Article]()
        Alamofire.request(url, method: .get, parameters: nil, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                
                let responeJSON = JSON(response.result.value!)
                let articlesJSON = responeJSON["DATA"]
                    for article in articlesJSON {
                        articles.append(Article(json: JSON(article.1)) as Article)
                    }
                let getIsSuccess = true
                completion(articles, getIsSuccess)
            }else{
                print("Respone Result is failed")
                let getIsSuccess = false
                completion(articles, getIsSuccess)
            }
        }
    }
    
    func getOneArticleById(id: String, completion: @escaping (Article) -> Void){
        let url = "http://api-ams.me/v1/api/articles/\(id)"
        let headers = ["Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        Alamofire.request(url, method: .get, parameters: nil, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articleJSON = responeJSON["DATA"]
                let article = Article(json: articleJSON) as Article
                completion(article)
            }else{
                print("Respone Result is failed")
            }
        }
    }
    
    func insertArticle(article: Article ,completion: @escaping (Article, Bool) -> Void){
        
        let parameters: Parameters = [
            "TITLE": article.title!,
            "DESCRIPTION": article.description!,
//          "AUTHOR": article.author?.id!,
            "CATEGORY_ID": article.category!.id!,
            "IMAGE": article.imageUrl!,
            "STATUS": article.status!,
        ]
        
        let url = "http://api-ams.me/v1/api/articles"
        let headers: HTTPHeaders = [
            "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
            "Accept" : "application/json"]
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articleJSON = responeJSON["DATA"]
                let article = Article(json: articleJSON) as Article
                
                let isInserted = true
                completion(article, isInserted)
            }else{
                let isInserted = false
                completion(article, isInserted)
                print("Insert Respone Result is failed")
            }
        }
    }
    
    
    func updateArticleById(article: Article ,completion: @escaping (Article, Bool) -> Void){
        
        let parameters: Parameters = [
            "TITLE": article.title!,
            "DESCRIPTION": article.description!,
            //  "AUTHOR": article.author?.id!,
            "CATEGORY_ID": article.category!.id!,
            "IMAGE": article.imageUrl!,
            "STATUS": article.status!,
            ]
        
        let url = "http://www.api-ams.me/v1/api/articles/\(article.id!)"
        let headers: HTTPHeaders = [
            "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
            "Accept" : "application/json"]
        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let isUpdated = true
                completion(article, isUpdated)
            }else{
                let isUpdated = false
                completion(article, isUpdated)
                print("Update Respone Result is failed")
            }
        }
    }
    
    
    func deleteOneArticleById(id:Int, completion: @escaping (Article, Bool) -> Void){
        var article: Article?
        
        let url = "http://api-ams.me/v1/api/articles/\(id)"
        let headers = ["Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        Alamofire.request(url, method: .delete, parameters: nil, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articlesJSON = responeJSON["DATA"]
                article = Article(json: articlesJSON)
                let isDeleted = true
                completion(article!, isDeleted)
            }else{
                let isDeleted = false
                completion(article!, isDeleted)
                print("Delete Respone Result is failed")
            }
        }
    }
    
    
    func uploadImage(image: UIImage, progressCompletion: @escaping (Double) -> Void, completion: @escaping (DataResponse<Any>) -> Void) {
        
        let imageData = image.jpegData(compressionQuality: 0.5)
        
        let url = "http://api-ams.me/v1/api/uploadfile/single"
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "FILE", fileName: "image.jpeg" , mimeType: "image/*")
            
        }, to: url ) { result in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { progress in
                    progressCompletion(progress.fractionCompleted)
                })
                upload.responseJSON(completionHandler: { response in
                    if response.result.isSuccess {
                        completion(response)
                    }
                })
                
            case .failure(_):
                print("Encoding failed")
            }
        }
    }
    
}
