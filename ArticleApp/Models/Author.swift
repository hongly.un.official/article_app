//
//  Author.swift
//  ArticleApp
//
//  Created by hongly on 12/19/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import SwiftyJSON

class Author {
    var id:Int?
    var name:String?
    var gender:String?
    var email:String?
    var telephone:String?
    var facebookId:String?
    var imageUrl:String?
    var status:String?
    
    init(){}
    init(id: Int?, name: String?, gender: String?, email: String?, telephone: String?, facebookId: String?, imageUrl: String?, status: String?){
        self.id = id
        self.name = name
        self.gender = gender
        self.email = name
        self.telephone = telephone
        self.facebookId = facebookId
        self.imageUrl = imageUrl
        self.status = status
        
    }
    init(json: JSON) {
        self.id = json["ID"].int
        self.name = json["NAME"].string
        self.gender = json["GENDER"].string
        self.email = json["EMAIL"].string
        self.telephone = json["TELEPHONE"].string
        self.facebookId = json["FACEBOOK_ID"].string
        self.imageUrl = json["IMAGE_URL"].string
        self.status = json["STATUS"].string
    }
}
