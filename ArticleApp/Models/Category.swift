//
//  Category.swift
//  ArticleApp
//
//  Created by hongly on 12/15/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import SwiftyJSON

class Category {
    var id:Int?
    var name:String?
    
     init(){}
    init(id: Int?, name: String?){
        self.id = id
        self.name = name
    }
    init(json: JSON) {
        self.id = json["ID"].int
        self.name = json["NAME"].string
    }
}
