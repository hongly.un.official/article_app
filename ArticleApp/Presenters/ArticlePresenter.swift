//
//  ArticlePresenter.swift
//  ArticleApp
//
//  Created by hongly on 12/24/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import UIKit

protocol ArticlePresenterProtocol {
    func didResponseArticles(articles: [Article])
    func didResponseOneArtilce(article: Article)
    func didInsertOneArtilce(article: Article)
    func didDeleteOneArtilce(article: Article)
    func didUpdateOneArtilce(article: Article)
    func didUploadImage(imageUrl: String)
}

class ArticlePresenter: ArticleServiceProtocol {

    var delegate: ArticlePresenterProtocol?
    var artilceService: ArticleService?
    
    init() {
        self.artilceService = ArticleService()
        self.artilceService?.delegate = self
    }
    
    func getAllArticles(page: Page) {
        artilceService?.getAllArticles(page: page)
    }
    
    func getOneArticleById(id: Int) {
        artilceService?.getOneArticleById(id: id)
    }
    func deleteOneArticleById(id:Int) {
        artilceService?.deleteOneArticleById(id: id)
    }
    func uploadImage(image: UIImage) {
        artilceService?.uploadImage(image: image)
    }
    
    func instertOneArticle(article: Article) {
        artilceService?.insertOneArticle(article: article)
    }
    
    func updateArticleById(article: Article) {
        artilceService?.updateArticleById(article: article)
    }
    
    func didResponseArticles(articles: [Article]) {
        self.delegate?.didResponseArticles(articles: articles)
    }
    
    func didResponseOneArtilce(article: Article) {
        self.delegate?.didResponseOneArtilce(article: article)
    }
    
    func didDeleteOneArtilce(article: Article) {
        self.delegate?.didDeleteOneArtilce(article: article)
    }
    
    func didUpdateOneArtilce(article: Article) {
        self.delegate?.didUpdateOneArtilce(article: article)
    }
    
    func didInsertOneArtilce(article: Article) {
        self.delegate?.didInsertOneArtilce(article: article)
    }
    
    func didUploadImage(imageUrl: String) {
        self.delegate?.didUploadImage(imageUrl: imageUrl)
    }
    
}
