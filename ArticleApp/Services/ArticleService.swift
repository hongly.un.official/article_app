//
//  ArticleService.swift
//  ArticleApp
//
//  Created by hongly on 12/24/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


protocol ArticleServiceProtocol {
    func didResponseArticles(articles: [Article])
    func didResponseOneArtilce(article: Article)
    func didInsertOneArtilce(article: Article)
    func didDeleteOneArtilce(article: Article)
    func didUpdateOneArtilce(article: Article)
    func didUploadImage(imageUrl: String)
}

class ArticleService {
    
    var delegate: ArticleServiceProtocol?
    
    func getAllArticles(page: Page){
        let url = "http://www.api-ams.me/v1/api/articles?page=\(page.currentPage)&limit=\(page.limit)"
        let headers = ["Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        var articles = [Article]()
        Alamofire.request(url, method: .get, parameters: nil, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articlesJSON = responeJSON["DATA"]
                for article in articlesJSON {
                    articles.append(Article(json: JSON(article.1)) as Article)
                }
                
                self.delegate?.didResponseArticles(articles: articles)
            }else{
                print("Respone Result is failed")
            }
        }
    }
    
    func getOneArticleById(id: Int){
        let url = "http://api-ams.me/v1/api/articles/\(id)"
        let headers = ["Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        Alamofire.request(url, method: .get, parameters: nil, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articleJSON = responeJSON["DATA"]
                let article = Article(json: articleJSON) as Article
                
                self.delegate?.didResponseOneArtilce(article: article)
            }else{
                print("Respone Result is failed")
            }
        }
    }
    
    func insertOneArticle(article: Article){
        
        let url = "http://api-ams.me/v1/api/articles"
        let headers: HTTPHeaders = [
            "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
            "Accept" : "application/json"]
        
        let parameters: Parameters = [
            "TITLE": article.title!,
            "DESCRIPTION": article.description!,
            "CATEGORY_ID": article.category!.id!,
            "IMAGE": article.imageUrl!,
            "STATUS": article.status!,
            ]
        
        Alamofire.request(url, method: .post, parameters: parameters, encoding: JSONEncoding.default, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articleJSON = responeJSON["DATA"]
                let article = Article(json: articleJSON) as Article
                
                self.delegate?.didInsertOneArtilce(article: article)
            }else{
                print("Insert Respone Result is failed")
            }
        }
    }
    
    func updateArticleById(article: Article){
        
        let parameters: Parameters = [
            "TITLE": article.title!,
            "DESCRIPTION": article.description!,
            "CATEGORY_ID": article.category!.id!,
            "IMAGE": article.imageUrl!,
            "STATUS": article.status!,
            ]
        
        let url = "http://www.api-ams.me/v1/api/articles/\(article.id!)"
        let headers: HTTPHeaders = [
            "Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ=",
            "Accept" : "application/json"]
        Alamofire.request(url, method: .put, parameters: parameters, encoding: JSONEncoding.default, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                
                self.delegate?.didUpdateOneArtilce(article: Article())
            }else{
                print("Update Respone Result is failed")
            }
        }
    }
    
    
    func deleteOneArticleById(id:Int){
        let url = "http://api-ams.me/v1/api/articles/\(id)"
        let headers = ["Authorization" : "Basic QU1TQVBJQURNSU46QU1TQVBJUEBTU1dPUkQ="]
        
 
        Alamofire.request(url, method: .delete, parameters: nil, headers: headers ).responseJSON { ( response ) in
            if response.result.isSuccess {
                let responeJSON = JSON(response.result.value!)
                let articlesJSON = responeJSON["DATA"]
                let article = Article(json: articlesJSON)
                self.delegate?.didDeleteOneArtilce(article: article)
            }else{
                print("Delete Respone Result is failed")
            }
        }
    }
    
    
    func uploadImage(image: UIImage) {
        
        let url = "http://api-ams.me/v1/api/uploadfile/single"
        
        let imageData = image.jpegData(compressionQuality: 0.5)
        Alamofire.upload(multipartFormData: { multipartFormData in
            multipartFormData.append(imageData!, withName: "FILE", fileName: "image.jpeg" , mimeType: "image/*")
        }, to: url ) { result in
            
            switch result {
            case .success(let upload, _, _):
                upload.uploadProgress(closure: { (progress) in
                    print(progress.fractionCompleted)
                })
                
                upload.responseJSON(completionHandler: { response in
                    if response.result.isSuccess {
                        let responeJSON = JSON(response.result.value!)
                        let imageUrl = responeJSON["DATA"].string
                        self.delegate?.didUploadImage(imageUrl: imageUrl!)
                        
                    }else{
                         print("Upload failed")
                    }
                })
            case .failure(_):
                print("Encoding failed")
            }
        }
    }
}
