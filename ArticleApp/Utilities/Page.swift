//
//  Page.swift
//  ArticleApp
//
//  Created by hongly on 12/19/18.
//  Copyright © 2018 hongly. All rights reserved.
//

import Foundation

class Page {
    static var shared = Page(currentPage: 1, limit: 6)
    
    var currentPage: Int
    var limit: Int
    
    init(currentPage: Int, limit: Int){
        self.currentPage = currentPage
        self.limit = limit
    }
}
